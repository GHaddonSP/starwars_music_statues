# Starwars musical statues

## How to play
Like standard music statues but instead of standing still when the music stops you must stand in one of four poses.

- Jedi - Arms raised as if holding a lightsaber above head
- Jabba the Hut - Arms out by side to make yourself as big as possible
- Darth Vader - The famous choke hold
- Yoda - Sitting down in a meditating pose

You will have three seconds before the randomiser stops on one of the four images representing the poses.

If the randomiser stops on your pose, or you run out of time you are out!

## How to use

### Start the game
Download the game files and open the game.html file in a compatible browser.

The game is best played on a 1920 x 1080 screen with the browser in full screen mode (F11 on Windows)

Press the space bar to start and stop the music.

To exit the game press F11 to exit fullscreen and close the browser. Alternatively press Alt + F4.

### Add your own Music
To add your own music add your music files to the music folder and update music.js in the same folder.

The music js should contain the file names of all your tracks like below:

```js
var music = [
    'song1.mp3',
    'song2.mp3',
    'song3.mp3'
];
```

### Starwars fonts
Fonts can be downloaded here: https://www.dafont.com/star-jedi.font