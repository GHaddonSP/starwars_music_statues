var images = ['jedi.jpg', 'darth.jpg', 'yoda.jpg', 'jabba.jpg'];
var danceImages = ['dance1.gif','dance2.gif','dance3.gif','dance4.gif','dance5.gif'];

var imagepath = "assets/images/";
var musicpath = "music/";

let track = 0;
let audio = new Audio(musicpath + music[track]);

// Listen for the music ended event, to play the next audio file
audio.addEventListener('ended', nextSong, false)


window.onload = function() {
    var element = document.getElementById("gameboard");
    element.classList.add("init");
}

document.onkeyup = function(e){
    if(e.keyCode === 32){
        var testClass = document.getElementById("gameboard").className;

        switch (testClass) {
            case "init":
                start(danceImages, audio);
                break;
            case "dance":
                cycle(images, audio);
                break;
            case "stopped":
                resume(danceImages, audio);
                break;
            default:

        }
    }
}

function cycle(images, audio) {
    audio.pause();
    var element = document.getElementById("gameboard");
    element.removeAttribute("class");
    element.classList.add("cycling");
    element.innerHTML = '<img id="rotator"></img>';
    var delayInSeconds = 1;

    var num = 0;
    var changeImage = function() {
        var len = images.length;
        document.getElementById("rotator").src = imagepath + images[num++];
        if (num == len) {
            num = 0;
        }
    };

    let timerCount = 3;
    document.getElementById("countdown").innerHTML = timerCount;

    var timer = document.getElementById("countdown");
    timer.removeAttribute("class");
    timer.classList.add("show");
    var countdownNumber = function() {
        timerCount--;
        document.getElementById("countdown").innerHTML = timerCount;
    };

    var cycleImages = setInterval(changeImage, delayInSeconds * 125);
    var countdown = setInterval(countdownNumber, 1000);

    setTimeout(function( ) {
        clearInterval( cycleImages );
        clearInterval( countdown );
        choose(images,audio);
        element.removeAttribute("class");
        element.classList.add("stopped");
        timer.removeAttribute("class");
    }, 3000);

}

function resume(danceImages, audio) {
    start(danceImages, audio);
}

function start(danceImages, audio) {
    var element = document.getElementById("gameboard");
    element.removeAttribute("class");
    element.classList.add("dance");
    element.innerHTML = '<img id="rotator"></img>';
    choose(danceImages)
    audio.play();
}

function choose(images) {
    var randomNum = Math.floor(Math.random() * images.length);
    document.getElementById("rotator").src = imagepath + images[randomNum];
}

function nextSong() {
    // Check for last audio file in the playlist
    if (track === music.length - 1) {
        track = 0;
    } else {
        track++;
    }

    // Change the audio element source
    audio.src = musicpath + music[track];
    audio.play();
}